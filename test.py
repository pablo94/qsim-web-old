import unittest
from operators import Register, Immediate, Direct
from operations import *
from exceptions import *
from architecture import MACHINE, Program
from memory import MEMORY
from config import CONFIG

class OperatorTestCase(unittest.TestCase):

	def test_register_max_id(self):
		r1 = Register(7)
		r1 = Register(0)
		with self.assertRaises(InvalidRegisterError):
			r12 = Register(8)
		with self.assertRaises(InvalidRegisterError):
			r12 = Register(-2)

	def test_register_value(self):
		r1 = Register(1)
		r1.value = "0x001a"
		self.assertEqual(r1.dec_value, 26)
		self.assertEqual(r1.value, "0x001a")

	def test_register_value_more_memory_cell_length(self):
		r1 = Register(1)

		with self.assertRaises(BitOverflowError) as ex:
			r1.value = "0xaa1aa"

		self.assertEqual("The value can't be assigned: 0xaa1aa", ex.exception.message)

	def test_register_value_not_valid_hexa(self):
		r1 = Register(1)

		with self.assertRaises(NotHexadecimalError) as ex:
			r1.value = "31fgtr"

		self.assertEqual("The value can't be assigned: 31fgtr", ex.exception.message)

	def test_immediate_value(self):
		i1 = Immediate("0x001a")

		self.assertEqual(i1.dec_value, 26)
		self.assertEqual(i1.value, "0x001a")

	def test_immediate_assign_value(self):
		i1 = Immediate("0x001a")
		with self.assertRaises(AssigningToImmediateError):
			i1.value = "0xffff"

	def test_immediate_value_more_memory_cell_length(self):
		with self.assertRaises(BitOverflowError) as ex:
			Immediate("0x430dd")

		self.assertEqual("The value can't be assigned: 0x430dd", ex.exception.message)

	def test_immediate_value_not_valid_hexa(self):
		with self.assertRaises(NotHexadecimalError) as ex:
			Immediate("31fgtr")

		self.assertEqual("The value can't be assigned: 31fgtr", ex.exception.message)

	def test_direct_value_set(self):
		direct = Direct("0x430d")
		direct.value = "0x000A"

		self.assertEqual(direct.value, hex(int("0x000A", 16)))

	def test_direct_overflow_value_set(self):
		direct = Direct("0x430d")
		with self.assertRaises(BitOverflowError) as ex:
			direct.value = "0xAAAAA"

		self.assertEqual("The value can't be assigned: 0xAAAAA", ex.exception.message)

	def test_direct_invalid_value_set(self):
		direct = Direct("0x430d")
		with self.assertRaises(NotHexadecimalError) as ex:
			direct.value = "0x0TAA"

		self.assertEqual("The value can't be assigned: 0x0TAA", ex.exception.message)

	def test_direct_value_get_unset(self):
		if not CONFIG.FETCH_ONLY_ASSIGNED:
			direct = Direct("0x430d")

			try:
				val = direct.value
			except Exception as e:
				raise e

class OperationTestCase(unittest.TestCase):

	def test_MOV_effect(self):
		r1 = Register(1)
		i1 = Immediate("0x001a")
		ins = MOV(r1, i1)

		ins.execute()

		self.assertEqual(r1.dec_value, 26)

	def test_ADD_effect(self):
		r1 = Register(1)
		r1.value = "0x001a"
		i1 = Immediate("0x001a")
		ins = ADD(r1, i1)

		ins.execute()

		self.assertEqual(r1.dec_value, 52)

	def test_ADD_effect_upper_limit(self):
		r1 = Register(1)
		r1.value = "0xffff"
		i1 = Immediate("0x0001")
		ins = ADD(r1, i1)

		ins.execute()

		self.assertEqual(r1.dec_value, 0)

	def test_SUB_effect(self):
		r1 = Register(1)
		r1.value = "0x001a"
		i1 = Immediate("0x001a")
		ins = SUB(r1, i1)

		ins.execute()

		self.assertEqual(r1.dec_value, 0)

	def test_SUB_effect_lower_limit(self):
		r1 = Register(1)
		r1.value = "0x0000"
		i1 = Immediate("0x0001")
		ins = SUB(r1, i1)

		ins.execute()

		self.assertEqual(r1.dec_value, (2**16)-1)

	def test_DIV_effect_entire(self):
		r1 = Register(1)
		r1.value = "0x0010"
		i1 = Immediate("0x0002")
		ins = DIV(r1, i1)

		ins.execute()

		self.assertEqual(r1.dec_value, 8)

	def test_DIV_effect(self):
		r1 = Register(1)
		r1.value = "0x0011"
		i1 = Immediate("0x0002")
		ins = DIV(r1, i1)

		ins.execute()

		self.assertEqual(r1.dec_value, 8)

	def test_MUL_effect(self):
		r1 = Register(1)
		r1.value = "0x0010"
		i1 = Immediate("0x0002")
		ins = MUL(r1, i1)

		ins.execute()

		self.assertEqual(r1.dec_value, 32)

	def test_MUL_effect_upper_limit(self):
		r1 = Register(1)
		r1.value = "0x1000"
		i1 = Immediate("0x0010")
		ins = MUL(r1, i1)

		ins.execute()

		self.assertEqual(r1.dec_value, 0)

	def test_ADD_effect_direct(self):
		r1 = Register(1)
		r1.value = "0x0003"
		direct = Direct("0xABAB")
		direct.value = "0x0003"
		ins = ADD(r1, direct)

		ins.execute()

		self.assertEqual(r1.value, '0x6')

	def test_ADD_effect_direct_upper_limit(self):
		r1 = Register(1)
		r1.value = "0xffff"
		direct = Direct("0xABAB")
		direct.value = "0x0001"
		ins = ADD(direct, r1)

		ins.execute()

		self.assertEqual(direct.dec_value, 0)

	def test_CALL_efect_immediate(self):
		imm = Immediate("0Xabab")

		ins = CALL(imm)
		MACHINE.execute(ins)

		self.assertEqual(hex(MACHINE.pc), "0xabab")

	def test_CALL_efect_direct(self):
		direct = Direct("0xABAB")
		direct.value = "0x0001"

		ins = CALL(direct)
		MACHINE.execute(ins)

		self.assertEqual(MACHINE.pc, 1)

	def test_CALL_efect_register(self):
		r = Register(1)
		r.value = "0x0001"

		ins = CALL(r)
		MACHINE.execute(ins)

		self.assertEqual(MACHINE.pc, 1)

	def test_instruction_target_cant_be_immediate(self):
		with self.assertRaises(ImmediateAsTargetError) as ex:
			ADD(Immediate("0Xabab"), Immediate("0Xabab"))
		with self.assertRaises(ImmediateAsTargetError) as ex:
			SUB(Immediate("0Xabab"), Immediate("0Xabab"))
		with self.assertRaises(ImmediateAsTargetError) as ex:
			MOV(Immediate("0Xabab"), Immediate("0Xabab"))
		with self.assertRaises(ImmediateAsTargetError) as ex:
			DIV(Immediate("0Xabab"), Immediate("0Xabab"))
		with self.assertRaises(ImmediateAsTargetError) as ex:
			MUL(Immediate("0Xabab"), Immediate("0Xabab"))

class AssembleTestCase(unittest.TestCase):
	def test_register_assemble(self):
		r = Register(0)
		self.assertEqual(r.assemble, "100000")
		r = Register(1)
		self.assertEqual(r.assemble, "100001")
		r = Register(2)
		self.assertEqual(r.assemble, "100010")
		r = Register(3)
		self.assertEqual(r.assemble, "100011")
		r = Register(4)
		self.assertEqual(r.assemble, "100100")
		r = Register(5)
		self.assertEqual(r.assemble, "100101")
		r = Register(6)
		self.assertEqual(r.assemble, "100110")
		r = Register(7)
		self.assertEqual(r.assemble, "100111")


	def test_register_value_assemble(self):
		r = Register(7)
		self.assertEqual(r.value_assemble, "")

	def test_immediate_assemble(self):
		imm = Immediate("0Xabab")
		self.assertEqual(imm.assemble, "000000")

	def test_immediate_value_assemble(self):
		imm = Immediate("0Xabab")
		self.assertEqual(imm.value_assemble, "1010101110101011")

	def test_direct_assemble(self):
		imm = Direct("0Xabab")
		self.assertEqual(imm.assemble, "001000")

	def test_direct_value_assemble(self):
		direct = Direct("0Xabab")
		direct.value = "0x0101"
		self.assertEqual(direct.value_assemble, "1010101110101011")

	def test_mov_assemble(self):
		r1 = Register(1)
		r1.value = "0x001a"
		direct = Direct("0xABAB")
		direct.value = "0x001a"
		self.assertEqual(MOV(r1, direct).assemble, '00011000010010001010101110101011')



	def test_instruction_store(self):
		r1 = Register(1)
		r1.value = "0x001a"
		direct = Direct("0xABAB")
		direct.value = "0x001a"
		MEMORY.store('0x0000', MOV(r1, direct).assemble)
		self.assertEqual(MEMORY.fetch('0x0000'), '0001100001001000')
		self.assertEqual(MEMORY.fetch('0x0001'), '1010101110101011')

	def test_program_assemble(self):
		r1 = Register(1)
		r1.value = "0x001a"
		direct = Direct("0xABAB")
		direct.value = "0x001a"
		ins = MOV(r1, direct)
		direct2 = Direct("0xFFFF")
		direct2.value = "0x0001"
		ins2 = MOV(direct2, r1)

		prg = Program([ins, ins2])

		MACHINE.assemble(prg)
		
		self.assertEqual(prg.start_address, 0)
		self.assertEqual(MEMORY.fetch('0x0000'), '0001100001001000')
		self.assertEqual(MEMORY.fetch('0x0001'), '1010101110101011')
		self.assertEqual(MEMORY.fetch('0x0002'), '0001001000100001')
		self.assertEqual(MEMORY.fetch('0x0003'), '1111111111111111')


class ExecutionTestCase(unittest.TestCase):

	def setUp(self):
		MEMORY.reset()
		MACHINE.reset()

	def test_program_execution(self):
		r1 = Register(1)
		direct = Direct("0xABAB")
		direct.value = "0x0003"
		ins = MOV(r1, Immediate("0x0003"))
		ins2 = ADD(r1, direct)

		prg = Program([ins, ins2])

		MACHINE.assemble(prg)
		MACHINE.start()
		self.assertEqual(MACHINE.state.registers[1].value, '0x6')
		self.assertEqual(direct.value, '0x3')

	def test_program_call_execution(self):
		r1 = Register(1)
		ins = CALL(Immediate("0x0000"))
		ins2 = ADD(r1, Immediate("0x0003"))
		
		ins3 = MOV(r1, Immediate("0x0003"))
		ins4 = MUL(r1, Immediate("0x0003"))
		ins5 = RET()

		rout = Program([ins3, ins4, ins5])
		prg = Program([ins, ins2])

		MACHINE.assemble(rout)
		MACHINE.assemble(prg)

		MACHINE.state.pc = prg.start_address
		MACHINE.start()

		self.assertEqual(MACHINE.state.registers[1].value, '0xc')

	def test_program_two_call_execution(self):
		r1 = Register(1)
		ins = CALL(Immediate("0x0000"))
		ins2 = ADD(r1, Immediate("0x0003"))
		
		ins3 = MOV(r1, Immediate("0x0003"))
		ins4 = CALL(Immediate("0x0006"))
		ins5 = RET()

		ins6 = MUL(r1, Immediate("0x0003"))
		ins7 = RET()

		rout = Program([ins3, ins4, ins5])
		rout2 = Program([ins6, ins7])
		prg = Program([ins, ins2])

		MACHINE.assemble(rout)
		MACHINE.assemble(rout2)
		MACHINE.assemble(prg)

		MACHINE.state.pc = prg.start_address
		MACHINE.start()

		self.assertEqual(MACHINE.state.registers[1].value, '0xc')

if __name__ == '__main__':
    unittest.main()