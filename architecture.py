from config import CONFIG
from memory import MEMORY
from operations import Operation
from state import State
from utils import *

class QComputer():
	def __init__(self):
		self.state = State()
		self.MAR = 0x0
		self._last_cell = 0

	def _fetch(self):
		self.MAR = hex(self.state.pc)
		self.MBR = MEMORY.fetch(self.MAR)
		self.state.pc += 1
		return self.MBR

	def _decode(self):
		self.IR = Operation.decode(self.MBR, self.state)

	def _execute(self):
		self.IR.execute(state=self.state)

	def execute(self, instruction):
		instruction.execute(state=self.state)

	def assemble(self, program):
		program.start_address = self._last_cell
		for instruction in program.instructions:
			self._last_cell = MEMORY.store(hex(self._last_cell), instruction.assemble)

		MEMORY.store(hex(self._last_cell), None)
		self._last_cell += 1

	def start(self):
		while self._fetch() is not None:
			self._decode()
			self._execute()

	@property
	def pc(self):
		return self.state.pc

	def reset(self):
		self.state.pc = 0
		self._last_cell = 0
	
class Program:
	def __init__(self, instructions=[]):
		self.instructions = instructions
		self._start_address = None
	
	@property
	def start_address(self):
		return self._start_address

	@start_address.setter
	def start_address(self, address):
		if not self.start_address:
			self._start_address = address
	
MACHINE = QComputer()

