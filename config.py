from utils import *

class Config:
	def __init__(self):
		self.ADDRESS_BUS_SIZE = 16
		self.DATA_BUS_SIZE = 16
		self.MEMORY_CELLS = 2**self.ADDRESS_BUS_SIZE
		self.REGISTER_COUNT = 8
		self.FETCH_ONLY_ASSIGNED = True


	@property
	def MAX_REGISTER_NUMBER(self):
		return self.REGISTER_COUNT-1

	@property
	def MAX_INT(self):
		return (2**self.DATA_BUS_SIZE)-1

	@property
	def MAX_CELL_ADDRESS(self):
		return (2**self.DATA_BUS_SIZE)-1

	def valid_address(self, address):
		return hex2dec(address) <= self.MAX_CELL_ADDRESS

	def valid_cell_value(self, value):
		return hex2dec(value) <= self.MAX_INT

	def valid_register(self, register):
		return 0 <= register <= self.MAX_REGISTER_NUMBER

CONFIG = Config()