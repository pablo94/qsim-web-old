[![coverage report](https://gitlab.com/pablo94/qsim-web/badges/master/coverage.svg)](https://gitlab.com/pablo94/qsim-web/commits/master)
[![coverage report](https://img.shields.io/badge/build-in%20process-orange.svg)](https://gitlab.com/pablo94/qsim-web)

_Este proyecto está en proceso de construcción_

# QSIM - Web

Un simulador para el lenguaje de bajo nivel **Q**, utilizado en la Universidad Nacional de Quilmes con propósitos educativos

### Q1 - La base
Esta iteración del lenguaje solo consta de Registros y dos modos de direccionamiento:

| Modo     | Código | Aclaración |
| -------- | ------ | ---------- |
| Registro | 100XXX | Dónde XXX es la codificación binaria del número de registro [0;7] |
| Inmediato | 000000 | - |

Además cuenta con las siguientes operaciones:

| Código | Efecto |
| ------ | ------ |
| **MUL** | destino ← destino * origen |
| **MOV** | destino ←  origen |
| **ADD** | destino ← destino + origen |
| **SUB** | destino ← destino + origen |
| **DIV** | destino ← destino + origen |


### Q2 - Accediendo a memoria
Como Q1 es muy limitado, por no poder hacer uso de la memoria, en esta iteración se agrega un nuevo modo de direccionamiento, quedando:

| Modo | Código | Aclaración |
| ------ | ------ | ------ |
| Registro | 100XXX | Dónde XXX es la codificación binaria del número de registro [0;7] |
| Inmediato | 000000 | - |
| *Directo* | *001000* | - |

