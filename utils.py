def dec2bin(dec, bits=16):
	return "{:0{bits}b}".format(dec, bits=bits)

def hex2dec(hexa):
	return int(hexa, 16)

def bin2dec(binn):
	return int(binn, 2)

def bin2hex(binn):
	return hex(bin2dec(binn))

def hex2bin(hexa):
	return dec2bin(hex2dec(hexa))