from config import CONFIG
from random import randint
from utils import *

class Memory:
	def __init__(self, cell_amount, cell_length, fetch_only_assigned):
		self.CELL_AMOUNT = cell_amount
		self.CELL_LENGTH = cell_length
		self.FETCH_ONLY_ASSIGNED = fetch_only_assigned
		self._memory = dict()

	def fetch(self, address):
		address = hex2dec(address)
		if self.FETCH_ONLY_ASSIGNED:
			return self._memory[address]
		else:
			return self._memory[address] if address in self._memory else self.dirty_cell_value


	@property
	def dirty_cell_value(self):
		return dec2bin(randint(0,  (2**self.CELL_LENGTH)-1))

	def store(self, address, value):
		address = hex2dec(address)
		if value is not None:
			instruction_cells_count = (len(value)//self.CELL_LENGTH) + 1
			for cell_index in range(0, instruction_cells_count):
				start = self.CELL_LENGTH*cell_index
				end = start + self.CELL_LENGTH
				if start < len(value):
					self._memory[address] = value[start:end]
					address += 1
			return address
		else:
			self._memory[address] = value


	def reset(self):
		self._memory = dict()

MEMORY = Memory(CONFIG.MEMORY_CELLS, CONFIG.DATA_BUS_SIZE, CONFIG.FETCH_ONLY_ASSIGNED)