from memory import MEMORY
from utils import *

class State:
	def __init__(self, *args, **kwargs):
		self._pc = 0x0
		self.stack = Stack()
		self.registers = dict()

	@property
	def pc(self):
		return self._pc

	@pc.setter
	def pc(self, pc):
		self._pc = pc
	@property
	def sp(self):
		return self.stack.sp

	@sp.setter
	def sp(self, sp):
		self.stack.sp = sp

	def has_register(self, number):
		return number in self.registers 

	def get_register(self, number):
		return self.registers[number]

	def init_register(self, register):
		self.registers[register.number] = register
		return self.get_register(register.number)

class Stack:
	def __init__(self, *args, **kwargs):
		self._sp = hex2dec("0xFFEF")

	@property
	def sp(self):
		return self._sp

	@sp.setter
	def sp(self, sp):
		self._sp = sp

	def push(self, value):
		MEMORY.store(hex(self.sp), value)
	
	def pop(self):
		return MEMORY.fetch(hex(self._sp))
