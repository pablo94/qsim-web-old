class InvalidRegisterError(Exception):
	def __init__(self):
		self.message = "Max number of register is 7"

class AssigningToImmediateError(Exception):
	def __init__(self):
		self.message = "You can't assign to an immediate"

class BitOverflowError(Exception):
	def __init__(self, value):
		self.message = "The value can't be assigned: " + value

class NotHexadecimalError(Exception):
	def __init__(self, value):
		self.message = "The value can't be assigned: " + value

class ImmediateAsTargetError(Exception):
	def __init__(self):
		self.message = "The target operand can't be an Immediate"

class MemoryAddressOutOfBoundsError(Exception):
	def __init__(self, hexa):
		self.message = "Invalid memory address: {0}".format(hexa)