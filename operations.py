from exceptions import ImmediateAsTargetError
from operators import Operator
from utils import *

class Operation:
	@classmethod
	def decode(cls, code, *args):
		operation = next(op for op in OPERATIONS if op.get_op_cd(code) == op.op_cd)
			
		operation.decode_operators(code, *args)

		return operation

	@classmethod
	def get_op_cd(cls, code):
		return code[:cls.op_cd_len()]

class BinaryOperation(Operation):
	def __init__(self, target=None, source=None):
		if target.__class__.__name__ is 'Immediate': 
			raise ImmediateAsTargetError()

		self.target = target
		self.source = source

	def decode_operators(self, code, *args):
		target = Operator.decode(code[4:10], *args)
		source = Operator.decode(code[10:16], *args)
		self.target = target
		self.source = source

	@classmethod
	def op_cd_len(cls):
		return 4

	@property
	def assemble(self):
		return "{0}{1}{2}{3}{4}".format(self.op_cd, self.target.assemble, self.source.assemble, self.target.value_assemble, self.source.value_assemble)

	def __str__(self):
		return '{0} {1} {2}'.format(self.__class__.__name__, self.target, self.source)

class MOV(BinaryOperation):
	def execute(self, *args, **kwargs):
		self.target.value = self.source.value

	@property
	def op_cd(self):
		return '0001'

class ADD(BinaryOperation):
	def execute(self, *args, **kwargs):
		self.target.value = hex((self.target.dec_value + self.source.dec_value)% 2**16) 

	@property
	def op_cd(self):
		return '0010'
	
class SUB(BinaryOperation):
	def execute(self, *args, **kwargs):
		self.target.value = hex((self.target.dec_value - self.source.dec_value)% 2**16) 

	@property
	def op_cd(self):
		return '0011'
	
class DIV(BinaryOperation):
	def execute(self, *args, **kwargs):
		self.target.value = hex((self.target.dec_value // self.source.dec_value)) 

	@property
	def op_cd(self):
		return '0111'
	
class MUL(BinaryOperation):
	def execute(self, *args, **kwargs):
		self.target.value = hex((self.target.dec_value * self.source.dec_value)% 2**16) 

	@property
	def op_cd(self):
		return '0000'
	
class CALL(Operation):
	def __init__(self, source=None):
		self.source = source

	def execute(self, *args, state=None, **kwargs):
		state.stack.push(dec2bin(state.pc))
		state.sp -= 1
		state.pc = hex2dec(self.source.value)

	@classmethod
	def op_cd_len(cls):
		return 4

	@property
	def assemble(self):
		return '{0}000000{1}{2}'.format(self.op_cd, self.source.assemble, self.source.value_assemble)

	@property
	def op_cd(self):
		return '1011'

	def decode_operators(self, code, *args):
		self.source = Operator.decode(code[10:16], *args)
	
class RET(Operation):
	def execute(self, *args, state=None, **kwargs):
		state.sp += 1
		state.pc = bin2dec(state.stack.pop())

	@classmethod
	def op_cd_len(cls):
		return 4

	@property
	def assemble(self):
		return '{0}000000000000'.format(self.op_cd)

	@property
	def op_cd(self):
		return '1100'

	def decode_operators(self, code, *args):
		pass

OPERATIONS = [MOV(), ADD(), SUB(), DIV(), MUL(), CALL(), RET()]