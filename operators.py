from exceptions import *
from config import CONFIG
from memory import MEMORY
from utils import *

class Operator:
	def __init__(self):
		self._value = ""

	@property
	def dec_value(self):
		return hex2dec(self.value)


	@classmethod
	def decode(cls, code, state):
		operator = next(op for op in OPERATORS if op.can_decode(code))
		operator = operator.decode(code, state)
		return operator

	def check_hex(self, hexa):
		try:
			hex2dec(hexa)
		except ValueError as e:
			raise NotHexadecimalError(hexa)

		except Exception as e:
			raise e
	def check_cell_address(self, hexa):
		if not CONFIG.valid_address(hexa):
			raise MemoryAddressOutOfBoundsError(hexa)

	def check_cell_value(self, hexa):
		if not CONFIG.valid_cell_value(hexa):
			raise BitOverflowError(hexa)

class Register(Operator):
	base_assemble = '100'

	def __init__(self, number):
		if not CONFIG.valid_register(number):
			raise InvalidRegisterError()
		self.number = number
		self._value = '0x0'

	@property
	def value(self):
		return self._value

	@value.setter
	def value(self, value):
		self.check_hex(value)
		self.check_cell_value(value)
		self._value = value

	@property
	def assemble(self):
		return self.base_assemble+self._get_binary()

	def _get_binary(self):
		return dec2bin(self.number, 3)

	@property
	def value_assemble(self):
		return ""

	@classmethod
	def can_decode(cls, code):
		return code[:3] == cls.base_assemble

	@classmethod
	def decode(self, code, state):
		key = bin2dec(code[3:7])
		return state.get_register(key) if state.has_register(key) else state.init_register(Register(key))

	def __str__(self):
		return 'R{0}'.format(self.number)

class Immediate(Operator):
	assemble = '000000'

	def __init__(self, value):
		self.check_hex(value)
		self.check_cell_value(value)
		self._value = value
	
	@property
	def value(self):
		return self._value

	@value.setter
	def value(self, value):
		raise AssigningToImmediateError()
	
	@property
	def value_assemble(self):
		return hex2bin(self._value)
	
	@classmethod
	def can_decode(cls, code):
		return code == cls.assemble

	@classmethod
	def decode(self, code, state):
		pc = hex(state.pc)
		state.pc += 1
		return Immediate(bin2hex(MEMORY.fetch(pc)))

	def __str__(self):
		return self.value


class Direct(Operator):
	assemble = '001000'
	def __init__(self, value):
		self.check_hex(value)
		self.check_cell_address(value)
		self._address = value
	
	@property
	def dec_value(self):
		return hex2dec(self.value)

	@property
	def value(self):
		return bin2hex(MEMORY.fetch(self._address))

	@value.setter
	def value(self, value):
		self.check_hex(value)
		self.check_cell_value(value)
		MEMORY.store(self._address, hex2bin(value))
	
	@property
	def value_assemble(self):
		return hex2bin(self._address)

	@classmethod
	def can_decode(cls, code):
		return code == cls.assemble

	@classmethod
	def decode(self, code, state):
		pc = hex(state.pc)
		state.pc += 1
		return Direct(bin2hex(MEMORY.fetch(pc)))

	def __str__(self):
		return '[{0}]'.format(self._address)
		
OPERATORS = [Register, Direct, Immediate]